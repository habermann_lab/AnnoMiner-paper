var classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds =
[
    [ "ConvertIDs", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a9642e9a641bcdd3b298962c74ad7f1f7", null ],
    [ "convertGeneNames", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#ad4695d3be6de43c651a4f44e338a2867", null ],
    [ "convertInputList", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a5f0b158cdc1f9b57a7886b527bf8b9c7", null ],
    [ "createDict", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a017e9e9be97466ba5da5ae3e9d365bae", null ],
    [ "getConvertedDict", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a863ddfb8bf6b8a6127e82b81f3a23957", null ],
    [ "printAttributes", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a2a8ff9c3f4d4f846f225742f10b81864", null ],
    [ "dict", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a6307556dddfd062486638fab9a152a4f", null ],
    [ "dictCustom", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a015ae2f20a8361713659f00632a278f6", null ],
    [ "dictRef", "classannominer_1_1convert_gene_i_ds_1_1_convert_i_ds.html#a7484a76e38ac7d72d584baeb04e7bc73", null ]
];