var classannominer_1_1_enrichment_result =
[
    [ "EnrichmentResult", "classannominer_1_1_enrichment_result.html#abfb7f556bd1fc09cfcad9f83b88c9a46", null ],
    [ "compareTo", "classannominer_1_1_enrichment_result.html#a2e6ff70350c1418a91bca2e87d0a97ce", null ],
    [ "equals", "classannominer_1_1_enrichment_result.html#afca71a334fbc7f88d8733613a8be95b9", null ],
    [ "getGeneListAsString", "classannominer_1_1_enrichment_result.html#ae70965451160fb00bca209ed2c67370b", null ],
    [ "hashCode", "classannominer_1_1_enrichment_result.html#a2357c340bddd501dc89e204efd299165", null ],
    [ "setCS", "classannominer_1_1_enrichment_result.html#a0011845ab75b00262d3a73e254045c11", null ],
    [ "setFdr", "classannominer_1_1_enrichment_result.html#a34b96abac91271b43794d5210d848d1b", null ],
    [ "toJSONArray", "classannominer_1_1_enrichment_result.html#a48849e7ae101d23f6ed14a98daf1e1f1", null ],
    [ "toString", "classannominer_1_1_enrichment_result.html#a2b43cfd56463a33de501a0d18d533eea", null ],
    [ "cs", "classannominer_1_1_enrichment_result.html#a5ab14a8f319dfce1ddda7ced5778dad3", null ],
    [ "experiment", "classannominer_1_1_enrichment_result.html#a547f005febdb524c05baf2c71348e281", null ],
    [ "fdr", "classannominer_1_1_enrichment_result.html#a9f50e3da4eab7fda5f6ea17d9d25cea7", null ],
    [ "genomehits", "classannominer_1_1_enrichment_result.html#a2ef170a08fe02e238903955a753c2c32", null ],
    [ "genomesize", "classannominer_1_1_enrichment_result.html#a31417e97aa89a9d31fbacde98b5056f5", null ],
    [ "listhits", "classannominer_1_1_enrichment_result.html#abfb52f014aec605a56c1a9442950bd05", null ],
    [ "listsize", "classannominer_1_1_enrichment_result.html#acc10995214d628d122cde54834d76b5c", null ],
    [ "positives", "classannominer_1_1_enrichment_result.html#a12914062b897776250e93f2623d63977", null ],
    [ "pval", "classannominer_1_1_enrichment_result.html#a02a5ab0fe412b7df803bd0934ebe88a6", null ],
    [ "score", "classannominer_1_1_enrichment_result.html#a7d93be0818887a3a07ede2479ba07d99", null ],
    [ "used_list", "classannominer_1_1_enrichment_result.html#a926d85370d64163dcb1a7b17256c9c47", null ]
];