var classannominer_1_1_oriented_region =
[
    [ "OrientedRegion", "classannominer_1_1_oriented_region.html#a1e866bcaa681b9b2d20e2453d27cc510", null ],
    [ "OrientedRegion", "classannominer_1_1_oriented_region.html#a2289b81c9030949c4c088d490c2f49aa", null ],
    [ "borders3PrimeTo", "classannominer_1_1_oriented_region.html#aadc8f6313d5cc9b821fcd5f1b2a418f0", null ],
    [ "borders5PrimeTo", "classannominer_1_1_oriented_region.html#a0c10e745216a2844978c9226296be548", null ],
    [ "end", "classannominer_1_1_oriented_region.html#ae9608da1ebec2670499d1929c003cefe", null ],
    [ "flankingRegion3p", "classannominer_1_1_oriented_region.html#aaaca7b74b1bd65cae843dba0849924ed", null ],
    [ "flankingRegion3p", "classannominer_1_1_oriented_region.html#af02c4b6fc2646a63228bae3e9c536853", null ],
    [ "flankingRegion5p", "classannominer_1_1_oriented_region.html#a97d4bf5bd3177256881cdca4d95f7914", null ],
    [ "flankingRegion5p", "classannominer_1_1_oriented_region.html#a83124434950cfcab0e23d04934c6ed93", null ],
    [ "pos3pBy", "classannominer_1_1_oriented_region.html#ab3548978722acd537dc52994b2630f0d", null ],
    [ "pos3pBy", "classannominer_1_1_oriented_region.html#a1f16b6d8c84699e4a8b2a2140b91a785", null ],
    [ "pos5pBy", "classannominer_1_1_oriented_region.html#a73021c93d3eda16221655275e091951d", null ],
    [ "pos5pBy", "classannominer_1_1_oriented_region.html#a81c10d7da6d2824be673fc8bf4a46aea", null ],
    [ "start", "classannominer_1_1_oriented_region.html#aa79fcfd28bb36e15edee5326478525de", null ],
    [ "strandToJSON", "classannominer_1_1_oriented_region.html#ad5a978232c23a0977c25f7e5fd81b701", null ],
    [ "strandToString", "classannominer_1_1_oriented_region.html#a6fbd89f9384235668966fc4ae57587df", null ],
    [ "toJSON", "classannominer_1_1_oriented_region.html#a3fe880910656775f488c00b94a9900fb", null ],
    [ "toTSV", "classannominer_1_1_oriented_region.html#a1f7b8a92ff19e986c2e82ca335ffcf91", null ],
    [ "strand", "classannominer_1_1_oriented_region.html#abd6c09a38dfc9979d46694ccfbbef697", null ]
];