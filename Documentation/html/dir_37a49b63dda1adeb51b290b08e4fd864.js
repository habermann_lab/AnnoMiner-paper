var dir_37a49b63dda1adeb51b290b08e4fd864 =
[
    [ "LongRangeInteractions.java", "_long_range_interactions_8java.html", [
      [ "LongRangeInteractions", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions" ]
    ] ],
    [ "ManageUploads.java", "_manage_uploads_8java.html", [
      [ "ManageUploads", "classannominer_1_1ui_1_1web_1_1servlet_1_1_manage_uploads.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_manage_uploads" ]
    ] ],
    [ "NearbyGenes.java", "_nearby_genes_8java.html", [
      [ "NearbyGenes", "classannominer_1_1ui_1_1web_1_1servlet_1_1_nearby_genes.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_nearby_genes" ]
    ] ],
    [ "PeakCorrelation.java", "_peak_correlation_8java.html", [
      [ "PeakCorrelation", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_correlation.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_correlation" ]
    ] ],
    [ "PeakToGene.java", "_peak_to_gene_8java.html", [
      [ "PeakToGene", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene" ]
    ] ],
    [ "TFScan.java", "_t_f_scan_8java.html", [
      [ "TFScan", "classannominer_1_1ui_1_1web_1_1servlet_1_1_t_f_scan.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_t_f_scan" ]
    ] ],
    [ "UploadsCheck.java", "_uploads_check_8java.html", [
      [ "UploadsCheck", "classannominer_1_1ui_1_1web_1_1servlet_1_1_uploads_check.html", "classannominer_1_1ui_1_1web_1_1servlet_1_1_uploads_check" ]
    ] ]
];