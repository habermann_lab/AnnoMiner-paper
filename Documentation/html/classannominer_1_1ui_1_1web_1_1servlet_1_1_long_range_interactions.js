var classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions =
[
    [ "annotateResults", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#a6f2433fcb248c62e7dfe8ba95bd00bc4", null ],
    [ "doGet", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#ae7968e7c15da2c0eb166b2b69d937a2f", null ],
    [ "doPost", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#ac518fcadea3ce1599d76db78acf4441e", null ],
    [ "geneIdConvertion", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#a6eeea4f93c7499a1df86bb6347f1b94d", null ],
    [ "geneResults2JSON", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#a2d469368c50bfd6cb550c59a011f4715", null ],
    [ "getLog2FC", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#ac52795c7564026f78693b19a8f34be9f", null ],
    [ "parseIntersections", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#ae5072f3e0bd786f5e2c288ed49ffd10e", null ],
    [ "printLongRangeInteractionsResultsJSON", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#afcafd02f9599fa7d13bfce7b6b4ff451", null ],
    [ "returnLog", "classannominer_1_1ui_1_1web_1_1servlet_1_1_long_range_interactions.html#a16658ec6064d56b66aaeea0ed6c0aefb", null ]
];