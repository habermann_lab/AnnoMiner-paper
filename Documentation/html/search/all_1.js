var searchData=
[
  ['batch',['batch',['../namespacegenerate_collections.html#aeadabfa03d0a8c6c47c2ccd6851a6ca7',1,'generateCollections']]],
  ['batchenrich',['BatchEnrich',['../classannominer_1_1batchupload_1_1_batch_enrich.html',1,'annominer.batchupload.BatchEnrich'],['../classannominer_1_1batchupload_1_1_batch_enrich.html#a165c1630f7cdbfab75c4fb9b87f54f3a',1,'annominer.batchupload.BatchEnrich.BatchEnrich()']]],
  ['batchenrich_2ejava',['BatchEnrich.java',['../_batch_enrich_8java.html',1,'']]],
  ['batchenrichment',['batchEnrichment',['../classannominer_1_1batchupload_1_1_batch_enrich.html#a1ec536ee76f2964f6d963d25ffc197d1',1,'annominer::batchupload::BatchEnrich']]],
  ['batchuploadandenrich',['BatchUploadAndEnrich',['../classannominer_1_1batchupload_1_1_batch_upload_and_enrich.html',1,'annominer::batchupload']]],
  ['batchuploadandenrich_2ejava',['BatchUploadAndEnrich.java',['../_batch_upload_and_enrich_8java.html',1,'']]],
  ['biosample',['biosample',['../namespacegenerate_collections.html#a08c716af5e319bb612541f3a4dcbcad7',1,'generateCollections']]],
  ['borders3primeto',['borders3PrimeTo',['../classannominer_1_1_oriented_region.html#aadc8f6313d5cc9b821fcd5f1b2a418f0',1,'annominer::OrientedRegion']]],
  ['borders5primeto',['borders5PrimeTo',['../classannominer_1_1_oriented_region.html#a0c10e745216a2844978c9226296be548',1,'annominer::OrientedRegion']]],
  ['buildlists',['buildLists',['../classannominer_1_1ui_1_1web_1_1servlet_1_1_nearby_genes.html#aa39b71009a85821d3f3cee6387b57c50',1,'annominer::ui::web::servlet::NearbyGenes']]],
  ['buildnearbygenes',['buildNearbyGenes',['../classannominer_1_1ui_1_1web_1_1servlet_1_1_nearby_genes.html#a020bd14ccb904f18d9970fa9d9c69d99',1,'annominer::ui::web::servlet::NearbyGenes']]]
];
