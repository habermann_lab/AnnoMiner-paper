var classannominer_1_1_gene_coverage =
[
    [ "GeneCoverage", "classannominer_1_1_gene_coverage.html#a2d986b2545111075b629aac664dcaff4", null ],
    [ "GeneCoverage", "classannominer_1_1_gene_coverage.html#a5eb488cf0668d7f52c765d5a3f425fbc", null ],
    [ "addGene", "classannominer_1_1_gene_coverage.html#a75eba265cb41fe5d847cc0897f6d5ee6", null ],
    [ "getF1", "classannominer_1_1_gene_coverage.html#a5b6447b071ed2576fa818e6edfc88a3b", null ],
    [ "getF2", "classannominer_1_1_gene_coverage.html#a8b30555c55914fd4910616670a6567b4", null ],
    [ "nextChrom", "classannominer_1_1_gene_coverage.html#a27d6747d6b88d41e3eebd7e62e3f8e86", null ],
    [ "printCoverageResultsJSON", "classannominer_1_1_gene_coverage.html#a977ab06c071123557174a73543790882", null ]
];