var classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene =
[
    [ "annotateResults", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#aac3f6e88dcb7070ea1be4eae34e280ed", null ],
    [ "doGet", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#adff8b570bff2344ae274c5b6ba98e3e9", null ],
    [ "doPost", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#aea906d5c682df41d9b989c3850c81644", null ],
    [ "genecheck", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#af3816d191049415c50bbcfbd9309acaa", null ],
    [ "geneIdConvertion", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#a688a16ad4e3e9ee273d4ebca337fff3d", null ],
    [ "geneResults2JSON", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#a1f05df21a14870f51085e53c59981d6e", null ],
    [ "getTargetIntervals", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#afa024bda7368e0653034590caac50db2", null ],
    [ "parseIntersections", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#a2d05074b05deeb168acd47fe0b2621d9", null ],
    [ "IDFile", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#ad79e332ac287170792aa451d0caa5a2f", null ],
    [ "testList", "classannominer_1_1ui_1_1web_1_1servlet_1_1_peak_to_gene.html#ab9fe76f07924b0c03a9eba30a1c70306", null ]
];