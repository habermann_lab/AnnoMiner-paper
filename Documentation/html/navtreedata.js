var NAVTREE =
[
  [ "AnnoMiner", "index.html", [
    [ "AnnoMiner web server", "md__anno_miner__r_e_a_d_m_e.html", null ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_2_mongo_d_a_o_8java.html",
"classannominer_1_1_genome_annotation.html",
"classannominer_1_1io_1_1database_1_1mongodb3_1_1_mongo_d_b_connector.html#a2fa3f8639b810577992dfe1ddb8846c3",
"generate_collections_8py.html#aa17d7e3db72d14fca5232b8f1b4a9f02"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';